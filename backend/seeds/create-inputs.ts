import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("lists").del();
    await knex("tasks").del();

    // Inserts seed entries
    await knex("lists").insert([
        { name: "Demo_List_1" },
        { name: "Demo_List_2" },
    ]);

    await knex("tasks").insert([
        { name: "Demo_Task_1" ,description:"Demo desc. 1",deadline:"2021-11-27T00:44:00.000Z",list_id:1},
        { name: "Demo_Task_2" ,description:"Demo desc. 2",deadline:"2021-11-23T00:44:00.000Z",list_id:1},
        { name: "Demo_Task_3" ,description:"Demo desc. 3",deadline:"2021-11-26T00:44:00.000Z",list_id:2},
        { name: "Demo_Task_4" ,description:"Demo desc. 4",deadline:"2021-11-26T00:44:00.000Z",list_id:2,completed:true},
    ]);
};
