import { env } from "./env";
import Knex from "knex";
const knexConfig = require("./knexfile");
export const knex = Knex(knexConfig[env.NODE_ENV]);
