import { Knex } from "knex";

export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable("lists");
    if (!hasTable) {
        await knex.schema.createTable("lists", (table) => {
            table.increments();
            table.string("name").notNullable();
        });
    }
}

export async function down(knex: Knex) {
    await knex.schema.dropTableIfExists("lists");
}
