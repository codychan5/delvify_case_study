import { Knex } from "knex";

export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable("tasks");
    if (!hasTable) {
        await knex.schema.createTable("tasks", (table) => {
            table.increments();
            table.string("name").notNullable();;
            table.string("description");
            table.dateTime("deadline");
            table.boolean("completed").defaultTo(false);
            table.boolean("deadline_email_sent").defaultTo(false);
            table.integer("list_id").notNullable();
            table
                .foreign("list_id")
                .references("lists.id");
        });
    }
}

export async function down(knex: Knex) {
    await knex.schema.dropTableIfExists("tasks");
}
