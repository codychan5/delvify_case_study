import express from "express";
import { Controller } from "../server";

const routes = express.Router();

routes.get("/get_all_lists", (req, res) =>
  Controller.get_all_lists(req, res)
);

routes.post("/get_all_uncompleted_tasks", (req, res) =>
  Controller.get_all_uncompleted_tasks(req, res)
);

routes.post("/get_all_completed_tasks", (req, res) =>
  Controller.get_all_completed_tasks(req, res)
);

routes.post("/create_list", (req, res) =>
  Controller.create_list(req, res)
);

routes.post("/delete_list", (req, res) =>
  Controller.delete_list(req, res)
);

routes.post("/create_task", (req, res) =>
  Controller.create_task(req, res)
);

routes.post("/update_task", (req, res) =>
  Controller.update_task(req, res)
);

routes.post("/move_single_task", (req, res) =>
  Controller.move_single_task(req, res)
);

routes.post("/move_batch_tasks", (req, res) =>
  Controller.move_batch_tasks(req, res)
);

routes.post("/delete_single_task", (req, res) =>
  Controller.delete_single_task(req, res)
);

routes.post("/delete_batch_tasks", (req, res) =>
  Controller.delete_batch_tasks(req, res)
);

routes.post("/complete_task", (req, res) =>
  Controller.complete_task(req, res)
);

export default routes;
