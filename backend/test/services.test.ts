import Knex from "knex";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["test"]);
import { services } from "../services/services";

describe("Services", () => {
  let Service: services;

  beforeAll(async () => {
    Service = new services(knex);
    await knex.migrate.latest();
    await knex.seed.run();
  });

  it("should get all lists", async () => {
    const result = await Service.get_all_lists();
    expect(result).toHaveLength(2);
  });

  it("should get all uncompleted tasks", async () => {
    const result = await Service.get_all_uncompleted_tasks(1);
    expect(result).toHaveLength(2);
  });

  it("should get all completed tasks", async () => {
    const result = await Service.get_all_completed_tasks(2);
    expect(result).toHaveLength(1);
  });

  it("should create a list", async () => {
    await Service.create_list("new list 3");
    const result = await Service.get_all_lists();
    expect(result).toHaveLength(3);
  });

  it("should delete a list", async () => {
    await Service.delete_list(1);
    await Service.delete_list(2);
    const result = await Service.get_all_lists();
    expect(result).toHaveLength(1);
  });

  it("should create a task", async () => {
    const current_date_time = new Date();
    await Service.create_task("Demo_Task_5","Demo desc. 5",current_date_time,3);
    const result = await Service.get_all_uncompleted_tasks(3);
    expect(result).toHaveLength(1);
  });

  it("should update a task", async () => {
    const current_date_time = new Date();
    await Service.update_task("Demo_Task_5_updated","Demo desc. 5",current_date_time,5);
    const result = await Service.get_all_uncompleted_tasks(3);
    expect(result[0].name).toBe("Demo_Task_5_updated");
  });
  
  it("should move a task", async () => {
    await Service.create_list("new list 4");
    await Service.move_single_task(5,4);
    const result = await Service.get_all_uncompleted_tasks(3);
    expect(result).toHaveLength(0);
  });

  it("should move tasks in batch", async () => {
    const current_date_time = new Date();
    await Service.create_task("Demo_Task_6","Demo desc. 6",current_date_time,4);
    await Service.move_batch_tasks([5,6],3);
    const result = await Service.get_all_uncompleted_tasks(3);
    expect(result).toHaveLength(2);
  });

  it("should delete a task", async () => {
    await Service.delete_single_task(5);
    const result = await Service.get_all_uncompleted_tasks(3);
    expect(result).toHaveLength(1);
  });

  it("should delete tasks in batch", async () => {
    const current_date_time = new Date();
    await Service.create_task("Demo_Task_7","Demo desc. 7",current_date_time,3);
    await Service.delete_batch_tasks([6,7]);
    const result = await Service.get_all_uncompleted_tasks(3);
    expect(result).toHaveLength(0);
  });

  it("should delete tasks in batch", async () => {
    const current_date_time = new Date();
    await Service.create_task("Demo_Task_8","Demo desc. 8",current_date_time,3);
    await Service.create_task("Demo_Task_9","Demo desc. 9",current_date_time,3);
    await Service.complete_task(8);
    const result = await Service.get_all_completed_tasks(3);
    expect(result).toHaveLength(1);
  });

  afterAll(async () => {
    await knex.migrate.rollback();
    await knex.destroy();
  });
});
