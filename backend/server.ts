import express from "express";
import cron from "node-cron";
import { services } from "./services/services";
import { controller } from "./controllers/controllers";
import routes from "./routes/routes";
import { env } from "./env";
import { knex } from "./db";
import cors from "cors";

const app = express();

app.use(
  cors({
    origin: ["http://localhost:3000"],
  })
);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

export const Controller = new controller(new services(knex));

app.use(routes);

//Task scheduling for every minute checking if any tasks passed deadline and then sent an email to user if there's any//

cron.schedule("* * * * *", async () => {
  try {
    const current_date_time = new Date();
    const overdue_tasks = await knex
      .select("*")
      .from("tasks")
      .where("deadline_email_sent", false)
      .andWhere("completed", false)
      .andWhere("deadline", "<=", current_date_time);
    if (overdue_tasks) {
      for (let task of overdue_tasks) {
        console.log(
          "Email: Task -",
          task.name,
          "has just passed its deadline!"
        );
        await knex("tasks")
          .update({
            deadline_email_sent: true,
          })
          .where("id", task.id);
      }
    }
  } catch (error) {
    console.log(error);
  }
});

const port = env.PORT ?? 8080;
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
