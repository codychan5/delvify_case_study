import { services } from "../services/services";
import { Request, Response } from "express";

export class controller {
  constructor(private service: services) {}

  get_all_lists = async (req: Request, res: Response) => {
    try {
      const result = await this.service.get_all_lists();
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  get_all_uncompleted_tasks = async (req: Request, res: Response) => {
    try {
      const { list_id } = req.body;
      const result = await this.service.get_all_uncompleted_tasks(list_id);
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  get_all_completed_tasks = async (req: Request, res: Response) => {
    try {
      const { list_id } = req.body;
      const result = await this.service.get_all_completed_tasks(list_id);
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  create_list = async (req: Request, res: Response) => {
    try {
      const { name } = req.body;
      const result = await this.service.create_list(name);
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  delete_list = async (req: Request, res: Response) => {
    try {
      const { list_id } = req.body;
      const result = await this.service.delete_list(list_id);
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  create_task = async (req: Request, res: Response) => {
    try {
      const { name, description, deadline, list_id } = req.body;
      const result = await this.service.create_task(
        name,
        description,
        deadline,
        list_id
      );
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  update_task = async (req: Request, res: Response) => {
    try {
      const { name, description, deadline, task_id } = req.body;
      const result = await this.service.update_task(
        name,
        description,
        deadline,
        task_id
      );
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  move_single_task = async (req: Request, res: Response) => {
    try {
      const { task_id, new_list_id } = req.body;
      const result = await this.service.move_single_task(task_id, new_list_id);
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  move_batch_tasks = async (req: Request, res: Response) => {
    try {
      const { task_ids_array, new_list_id } = req.body;
      const result = await this.service.move_batch_tasks(
        task_ids_array,
        new_list_id
      );
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  delete_single_task = async (req: Request, res: Response) => {
    try {
      const { task_id } = req.body;
      const result = await this.service.delete_single_task(task_id);
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  delete_batch_tasks = async (req: Request, res: Response) => {
    try {
      const { task_ids_array } = req.body;
      const result = await this.service.delete_batch_tasks(task_ids_array);
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };

  complete_task = async (req: Request, res: Response) => {
    try {
      const { task_id } = req.body;
      const result = await this.service.complete_task(task_id);
      if (result.success){
        console.log("Email: You have just completed a task!");
      }
      res.json(result);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  };
}