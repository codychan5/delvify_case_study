import { Knex } from "knex";

export class services {
  constructor(private knex:Knex) {}
  //for getting all lists//
  async get_all_lists() {
    try {
      const all_lists = await this.knex.select("*").from("lists");
      return all_lists;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for getting all uncompleted tasks within a list//
  async get_all_uncompleted_tasks(list_id: number) {
    try {
      const all_uncompleted_tasks = await this.knex
        .select("*")
        .from("tasks")
        .where("list_id", list_id)
        .andWhere("completed", false);
      return all_uncompleted_tasks;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for getting all completed tasks within a list//
  async get_all_completed_tasks(list_id: number) {
    try {
      const all_completed_tasks = await this.knex
        .select("*")
        .from("tasks")
        .where("list_id", list_id)
        .andWhere("completed", true);
      return all_completed_tasks;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for creating a new list//
  async create_list(name: string) {
    try {
      await this.knex.insert({ name: name }).into("lists");
      return { success: "insert success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for deleteing an existing list//
  async delete_list(list_id: number) {
    try {
      await this.knex("tasks").where("list_id", list_id).del();
      await this.knex("lists").where("id", list_id).del();
      return { success: "delete success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for creating a new task//
  async create_task(
    name: string,
    description: string,
    deadline: Date,
    list_id: number
  ) {
    try {
      await this.knex
        .insert({
          name: name,
          description: description,
          deadline: deadline,
          list_id: list_id,
        })
        .into("tasks");
      return { success: "insert success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for updating a task//
  async update_task(
    name: string,
    description: string,
    deadline: Date,
    task_id: number
  ) {
    try {
      await this.knex("tasks")
        .update({
          name: name,
          description: description,
          deadline: deadline,
        })
        .where("id", task_id);
      return { success: "update success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for moving a single task to a different list//
  async move_single_task(task_id: number, new_list_id: number) {
    try {
      await this.knex("tasks")
        .update({
          list_id: new_list_id,
        })
        .where("id", task_id);
      return { success: "update success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for moving multiple tasks in batch to a different list//
  async move_batch_tasks(task_ids_array: Array<number>, new_list_id: number) {
    try {
      for (let id of task_ids_array) {
        await this.knex("tasks")
          .update({
            list_id: new_list_id,
          })
          .where("id", id);
      }
      return { success: "update success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for deleting a single task//
  async delete_single_task(task_id: number) {
    try {
      await this.knex("tasks").where("id", task_id).del();
      return { success: "delete success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for deleting tasks in batch//
  async delete_batch_tasks(task_ids_array: Array<number>) {
    try {
      for (let id of task_ids_array) {
        await this.knex("tasks").where("id", id).del();
      }
      return { success: "delete success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for completing a task//
  async complete_task(task_id: number) {
    try {
      await this.knex("tasks")
        .update({
          completed: true,
        })
        .where("id", task_id);
      return { success: "update success" };
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
