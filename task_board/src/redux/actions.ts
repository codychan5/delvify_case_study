export function update_other_list(list_id: number) {
  return {
    type: "@@UPDATE_OTHER_LIST" as const,
    list_id,
  };
}
export type Actions = ReturnType<typeof update_other_list>;
