import { Actions } from "./actions";
import produce from "immer";
import { RootState } from "../store";

const initialState: RootState = {
  list_id_to_update: undefined,
}

export function Reducer(state: RootState = initialState, action: Actions) {
  if (action.type === "@@UPDATE_OTHER_LIST") {
    return produce(state, (state) => {
      state.list_id_to_update = action.list_id
    });
  }
  return state;
}