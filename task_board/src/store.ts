import { createStore } from "redux";
import { Reducer } from "./redux/reducer";

export interface RootState {
  list_id_to_update: number | undefined
}

export const store = createStore(Reducer);
