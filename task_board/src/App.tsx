import { useCallback, useEffect, useState } from "react";
import AppStyles from "./App.module.css";
import LIST from "./components/list";
import CREATING_NEW_LIST from "./components/creating_new_list";
import APP_BAR from "./components/app_bar";
import { list_type } from "./components/interfaces/types";

function App() {
  const [lists, set_lists] = useState<list_type[]>([]);
  const [creating_new_list, set_creating_new_list] = useState(false);

  useEffect(() => {
    async function load() {
      const res = await fetch("http://localhost:8080/get_all_lists");
      const json = await res.json();
      console.log(json);
      set_lists(json);
    }
    load();
  }, [set_lists, creating_new_list, 
  ]);

  const handle_create_list_close = useCallback(() => {
    set_creating_new_list(false);
  }, [set_creating_new_list]);

  const create_list = useCallback(() => {
    set_creating_new_list(true);
  }, [set_creating_new_list]);

  const delete_list = useCallback(
    async (list_id) => {
      const body = { list_id };
      await fetch("http://localhost:8080/delete_list", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      const res = await fetch("http://localhost:8080/get_all_lists");
      const json = await res.json();
      console.log(json);
      set_lists(json);
    },
    [set_lists]
  );

  return (
    <div className={AppStyles.app}>
      <div className={AppStyles.title_bar}>
        <APP_BAR create_list_onclick={create_list}></APP_BAR>
      </div>
      <div className={AppStyles.main}>
        {!lists[0] ? (
          <h1 className={AppStyles.add_list_reminder}>
            Start by clicking the "+" button at the left upper corner to create
            a list!
          </h1>
        ) : (
          false
        )}
        {creating_new_list === true ? (
          <CREATING_NEW_LIST
            open_creating_task={creating_new_list}
            finish_creating_list={handle_create_list_close}
          />
        ) : (
          false
        )}
        {lists.map((list) => (
          <LIST
            name={list.name}
            list_on_delete={() => {
              delete_list(list.id);
            }}
            list_id={list.id}
            lists={lists}
            key={list.id}
          />
        ))}
      </div>
    </div>
  );
}

export default App;
