import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from "@material-ui/core";
import { useState, useCallback } from "react";

function CREATING_NEW_LIST(props: {
  open_creating_task: boolean;
  finish_creating_list: () => void;
}) {
  const [list_name, set_list_name] = useState("");
  const handle_submit = useCallback(async () => {
    const name = list_name
    const body = { name };
    await fetch('http://localhost:8080/create_list', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
    });
  }, [list_name]);

  const handle_name_change = (event: React.ChangeEvent<HTMLInputElement>) => {
    set_list_name(event.target.value);    
  };

  return (
    <Dialog
      open={props.open_creating_task}
      onClose={props.finish_creating_list}
    >
      <DialogTitle>Create a new list</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To create a list, please enter the name of the list.
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Name"
          type="name"
          fullWidth
          variant="standard"
          onChange={handle_name_change}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.finish_creating_list}>Cancel</Button>
        <Button
          onClick={() => {
            props.finish_creating_list();
            handle_submit();
          }}
        >
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default CREATING_NEW_LIST;
