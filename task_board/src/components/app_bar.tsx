import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import CREATE_LIST from "./buttons/create_list";

function APP_BAR(props: {
  create_list_onclick: () => void}) {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <CREATE_LIST onClick={props.create_list_onclick}></CREATE_LIST>
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            TASK BOARD
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default APP_BAR;
