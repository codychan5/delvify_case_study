import AppStyles from "./List.module.css";
import DELETE_LIST from "./buttons/delete_list";
import { useState, useCallback, useEffect } from "react";
import TASK from "./task";
import COMPLETED_TASK from "./completed_task";
import CREATING_TASK from "./creating_task";
import CREATE_TASK from "./buttons/create_task";
import BATCH_EDIT from "./buttons/batch_edit";
import EXIT_BATCH_EDIT from "./buttons/exit_batch_edit";
import BATCH_DELETE_TASK from "./buttons/batch_delete_tasks";
import BATCH_MOVE_TASKS from "./buttons/batch_move_tasks";
import { completed_task_type } from "./interfaces/types";
import { task_type } from "./interfaces/types";
import { list_type } from "./interfaces/types";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store";
import { update_other_list } from "../redux/actions";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@material-ui/core";

function LIST(props: {
  name: string;
  list_id: number;
  list_on_delete: () => void;
  lists: list_type[];
}) {
  const list_id_to_update = useSelector(
    (state: RootState) => state.list_id_to_update
  );
  const dispatch = useDispatch();

  const [tasks, set_tasks] = useState<task_type[]>([]);
  const [completed_tasks, set_completed_tasks] = useState<
    completed_task_type[]
  >([]);
  const [batch_selected_items, set_batch_selected_items] = useState<number[]>(
    []
  );
  const [single_selected_task, set_single_selected_task] = useState<
    number | undefined
  >(undefined);
  const [batch_edit_mode, set_batch_edit_mode] = useState(false);
  const [creating_new_task, set_creating_new_task] = useState(false);
  const [batch_moving_tasks, set_batch_moving_tasks] = useState(false);
  const [moving_single_task, set_moving_single_task] = useState(false);
  const [list_updator, set_list_updator] = useState<number | null>(null);
  const [target_move_to_list, set_target_move_to_list] = useState<number>(
    props.list_id
  );
  const [reloader, set_reloader] = useState(0);

  useEffect(() => {
    if (props.list_id === list_id_to_update) {
      console.log("list_id_to_update", list_id_to_update);
      set_list_updator(list_id_to_update);
      load_uncompleted_task();
    }
  }, [set_list_updator, list_id_to_update, props.list_id]);

  const load_uncompleted_task = useCallback(async () => {
    const list_id = props.list_id;
    const body = { list_id };
    const res = await fetch("http://localhost:8080/get_all_uncompleted_tasks", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    const json = await res.json();
    console.log("load_uncompleted_task", json);
    set_tasks(json);
  }, [props.list_id, set_tasks]);

  useEffect(() => {
    load_uncompleted_task();
  }, [
    props.list_id,
    creating_new_task,
    batch_moving_tasks,
    moving_single_task,
    reloader,
    list_updator,
    set_tasks,
  ]);

  useEffect(() => {
    async function load_completed_task() {
      const list_id = props.list_id;
      const body = { list_id };
      const res = await fetch("http://localhost:8080/get_all_completed_tasks", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      const json = await res.json();
      console.log("load_completed_task", json);
      set_completed_tasks(json);
    }
    load_completed_task();
  }, [props.list_id, reloader]);

  //enter / exit batch edit mode //

  const enter_batch_edit_mode = useCallback(() => {
    set_batch_edit_mode(true);
  }, [set_batch_edit_mode]);

  const exit_batch_edit_mode = useCallback(() => {
    set_batch_edit_mode(false);
    set_batch_selected_items([]);
  }, [set_batch_edit_mode, set_batch_selected_items]);

  //enter / exit create new task mode //

  const started_create_new_task = useCallback(() => {
    set_creating_new_task(true);
  }, [set_creating_new_task]);

  const finished_creating_task = useCallback(() => {
    set_creating_new_task(false);
  }, [set_creating_new_task]);

  //enter / exit batch moving task mode //

  const started_batch_moving_task = useCallback(() => {
    set_batch_moving_tasks(true);
  }, [set_batch_moving_tasks]);

  const finished_batch_moving_tasks = useCallback(() => {
    set_batch_moving_tasks(false);
  }, [set_batch_moving_tasks]);

  //enter / exit moving task mode //

  const started_moving_single_task = useCallback(() => {
    set_moving_single_task(true);
  }, [set_moving_single_task]);

  const finished_moving_single_task = useCallback(() => {
    set_moving_single_task(false);
    set_single_selected_task(undefined);
  }, [set_moving_single_task]);

  //handling reloading the list after updating a task//
  const task_updated = useCallback(async () => {
    console.log("task_updated & list reloaded");
    let new_reloader: number = reloader;
    new_reloader += 1;
    set_reloader(new_reloader);
  }, [set_reloader, reloader]);

  //handling moving batch tasks and updating backend//
  const moving_batach_task_submit = useCallback(async () => {
    const body = {
      task_ids_array: batch_selected_items,
      new_list_id: target_move_to_list,
    };
    console.log("moving_batach_task_submit:", body);

    await fetch("http://localhost:8080/move_batch_tasks", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });

    console.log("dispatched:", target_move_to_list);

    dispatch(update_other_list(target_move_to_list));
    exit_batch_edit_mode();
  }, [
    batch_selected_items,
    target_move_to_list,
    dispatch,
    exit_batch_edit_mode,
  ]);

  //handling moving a task and updating backend//
  const moving_single_task_submit = useCallback(async () => {
    const body = {
      task_id: single_selected_task,
      new_list_id: target_move_to_list,
    };
    console.log("moving_single_task_submit:", body);

    await fetch("http://localhost:8080/move_single_task", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });

    console.log("dispatched:", target_move_to_list);

    dispatch(update_other_list(target_move_to_list));
  }, [target_move_to_list, single_selected_task, dispatch]);

  //handling deleting single task updating UI and backend//

  const filter_task_by_id = useCallback(
    (tasks_given, filtered_id) => {
      const temp_tasks = [];
      for (let item of tasks_given) {
        if (item.id !== filtered_id) {
          temp_tasks.push(item);
        }
      }
      return temp_tasks;
    },
    [set_tasks]
  );

  const delete_single_task = useCallback(
    async (task_id: number) => {
      const body = {
        task_id: task_id,
      };
      console.log("deleting_single_task_submit:", body);

      await fetch("http://localhost:8080/delete_single_task", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
    },
    [single_selected_task]
  );

  //handling deleting tasks in batch updating UI and backend//

  const batch_filter_tasks = useCallback(
    (filtered_ids) => {
      const new_tasks = tasks;
      const temp_tasks = [];
      for (let item of new_tasks) {
        if (filtered_ids.indexOf(item.id) === -1) {
          temp_tasks.push(item);
        }
      }
      set_tasks(temp_tasks);
    },
    [set_tasks, tasks]
  );

  const delete_batch_tasks = useCallback(async () => {
    const body = {
      task_ids_array: batch_selected_items,
    };
    console.log("deleting_batach_tasks_submit:", body);

    await fetch("http://localhost:8080/delete_batch_tasks", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    exit_batch_edit_mode();
  }, [batch_selected_items, exit_batch_edit_mode]);

  //handling comleting single task updating UI and backend//

  const complete_task = useCallback(
    async (task_id: number) => {
      const body = {
        task_id: task_id,
      };
      console.log("completing_task_submit:", body);

      await fetch("http://localhost:8080/complete_task", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
    },
    [single_selected_task]
  );

  //handling dropdown selection for moving tasks//

  const handle_change = useCallback(
    (event: SelectChangeEvent<typeof target_move_to_list>) => {
      set_target_move_to_list(Number(event.target.value));
      console.log("to_list_onchange", event.target.value);
    },
    [set_target_move_to_list]
  );

  return (
    <div className={AppStyles.list_main}>
      <h1>{props.name}</h1>
      {batch_edit_mode === true ? (
        <div className={AppStyles.list_button_group}>
          <EXIT_BATCH_EDIT onClick={exit_batch_edit_mode}></EXIT_BATCH_EDIT>
          <BATCH_DELETE_TASK
            onClick={() => {
              const new_batch_selected_items = batch_selected_items;
              batch_filter_tasks(new_batch_selected_items);
              delete_batch_tasks();
            }}
          ></BATCH_DELETE_TASK>
          <BATCH_MOVE_TASKS
            onClick={started_batch_moving_task}
          ></BATCH_MOVE_TASKS>
        </div>
      ) : (
        <div className={AppStyles.list_button_group}>
          <CREATE_TASK onClick={started_create_new_task}></CREATE_TASK>
          <BATCH_EDIT onClick={enter_batch_edit_mode}></BATCH_EDIT>
          <DELETE_LIST onClick={props.list_on_delete}></DELETE_LIST>
        </div>
      )}
      {batch_moving_tasks === true ? (
        <Dialog open={batch_moving_tasks} onClose={finished_batch_moving_tasks}>
          <DialogTitle>Moving Task</DialogTitle>
          <DialogContent>
            <DialogContentText>
              To move tasks to a different list,
              <br />
              please select which list you would like to move to.
            </DialogContentText>
            <Box component="form" sx={{ display: "flex", flexWrap: "wrap" }}>
              <FormControl sx={{ m: 1, minWidth: 120 }}>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={target_move_to_list}
                  onChange={handle_change}
                >
                  {props.lists.map((list) => (
                    <MenuItem value={list.id}>{list.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={finished_batch_moving_tasks}>Cancel</Button>
            <Button
              onClick={() => {
                moving_batach_task_submit();
                finished_batch_moving_tasks();
              }}
            >
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      ) : (
        false
      )}
      {moving_single_task === true ? (
        <Dialog open={moving_single_task} onClose={finished_moving_single_task}>
          <DialogTitle>Moving Task</DialogTitle>
          <DialogContent>
            <DialogContentText>
              To move a task to a different list,
              <br />
              please select which list you would like to move to.
            </DialogContentText>
            <Box component="form" sx={{ display: "flex", flexWrap: "wrap" }}>
              <FormControl sx={{ m: 1, minWidth: 120 }}>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={target_move_to_list}
                  onChange={handle_change}
                >
                  {props.lists.map((list) => (
                    <MenuItem value={list.id}>{list.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={finished_moving_single_task}>Cancel</Button>
            <Button
              onClick={() => {
                moving_single_task_submit();
                finished_moving_single_task();
              }}
            >
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      ) : (
        false
      )}
      <div className={AppStyles.tasks_container}>
        {creating_new_task ? (
          <CREATING_TASK
            onclick_confirm={finished_creating_task}
            onclick_cancel={finished_creating_task}
            list_id={props.list_id}
          ></CREATING_TASK>
        ) : (
          false
        )}
        {tasks.map((task) => (
          <TASK
            name={task.name}
            description={task.description}
            deadline={task.deadline}
            confirm_update={task_updated}
            batch_edit_mode={batch_edit_mode}
            task_id={task.id}
            onclick_complete_task={() => {
              complete_task(task.id)
              const new_tasks = tasks;
              const new_tasks_updated = filter_task_by_id(new_tasks, task.id);
              set_tasks(new_tasks_updated);
              const new_completed_tasks = completed_tasks;
              const new_completed_tasks_updated = new_completed_tasks.concat(task)
              set_completed_tasks(new_completed_tasks_updated);
            }}
            onclick_move_task={() => {
              set_single_selected_task(task.id);
              started_moving_single_task();
            }}
            onclick_delete_task={() => {
              delete_single_task(task.id);
              const new_tasks = tasks;
              const new_tasks_updated = filter_task_by_id(new_tasks, task.id);
              set_tasks(new_tasks_updated);
            }}
            batch_select_onclick={() => {
              const new_batch_selected_items = batch_selected_items;
              if (new_batch_selected_items === []) {
                const new_batch_selected_items_updated = [task.id];
                set_batch_selected_items(new_batch_selected_items_updated);
              } else if (new_batch_selected_items.indexOf(task.id) === -1) {
                const new_batch_selected_items_updated =
                  new_batch_selected_items.concat(task.id);
                set_batch_selected_items(new_batch_selected_items_updated);
              } else {
                const new_batch_selected_items_updated =
                  new_batch_selected_items.filter(
                    (all_id: number) => all_id !== task.id
                  );
                set_batch_selected_items(new_batch_selected_items_updated);
              }
            }}
            key={task.id}
          />
        ))}
        {completed_tasks[0] ? <h2>- Completed Tasks -</h2> : false}
        {completed_tasks.map((task) => (
          <COMPLETED_TASK
            name={task.name}
            description={task.description}
            deadline={task.deadline}
            key={task.id}
          />
        ))}
      </div>
    </div>
  );
}

export default LIST;
