import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  InputLabel,
  OutlinedInput,
  Select,
  SelectChangeEvent,
} from "@material-ui/core";
import { useCallback, useState } from "react";
import { list_type } from "./interfaces/types";

function MOVING_SINGLE_TASK(props: {
  open_moving_task: boolean;
  finish_moving_task: () => void;
  lists: list_type[];
  task_id: number;
}) {
  const [moving_to_list, set_moving_to_list] = useState<string | number | null>(
    null
  );

  const handle_change = useCallback(
    (event: SelectChangeEvent<typeof moving_to_list>) => {
      set_moving_to_list(event.target.value);
      console.log(event.target.value);
    },
    [set_moving_to_list]
  );

  const handle_move_task_submit = useCallback(
    async() => {
      console.log("moving_single_task_to_list:",moving_to_list);
      const body = {
        task_id: props.task_id,
        new_list_id: moving_to_list
      };
      await fetch("http://localhost:8080/update_task", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
    },
    [moving_to_list]
  );

  return (
    <Dialog open={props.open_moving_task} onClose={props.finish_moving_task}>
      <DialogTitle>Moving Task</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To move a task / tasks to a different list,
          <br />
          please select which list you would like to move to.
        </DialogContentText>
        <Box component="form" sx={{ display: "flex", flexWrap: "wrap" }}>
          <FormControl sx={{ m: 1, minWidth: 120 }}>
            <InputLabel htmlFor="demo-dialog-native">Lists</InputLabel>
            <Select
              native
              value={moving_to_list}
              onChange={handle_change}
              input={<OutlinedInput label="Lists" id="demo-dialog-native" />}
            >
              {props.lists.map((list) => (
                <option value={list.id}>{list.name}</option>
              ))}
            </Select>
          </FormControl>
        </Box>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.finish_moving_task}>Cancel</Button>
        <Button onClick={()=>{
          handle_move_task_submit();
          props.finish_moving_task()}}>Confirm</Button>
      </DialogActions>
    </Dialog>
  );
}

export default MOVING_SINGLE_TASK;
