import {
  Button,
  CardActions,
  CardContent,
  Checkbox,
  TextField,
  Typography,
} from "@material-ui/core";
import { useState, useCallback, Fragment } from "react";
import AppStyles from "./Task.module.css";
import DELETE_TASK from "./buttons/delete_task";
import COMPLETE_TASK from "./buttons/complete_task";
import UPDATE_TASK from "./buttons/update_task";
import MOVE_TASK from "./buttons/move_task";
import { DateTimePicker } from "@material-ui/lab";
import TIME_HANDLER from "./time_handler";

function TASK(props: {
  name: string;
  description: string;
  deadline: Date;
  batch_edit_mode: boolean;
  task_id: number;
  batch_select_onclick: () => void;
  onclick_complete_task: () => void;
  onclick_move_task: () => void;
  onclick_delete_task: () => void;
  confirm_update: () => void;
}) {
  const [updating_the_task, set_updating_the_task] = useState(false);
  const [title_to_be_updated, set_title_to_be_updated] = useState(props.name);
  const [date_to_be_updated, set_date_to_be_updated] = useState<Date | null>(
    props.deadline
  );
  const [description_to_be_updated, set_description_to_be_updated] = useState(
    props.description
  );

  const start_update_the_task = useCallback(() => {
    set_updating_the_task(true);
  }, [set_updating_the_task]);
  const finish_updating_task = useCallback(() => {
    set_updating_the_task(false);
  }, [set_updating_the_task]);

  const handle_update_confirm = useCallback(async () => {
    console.log(
      "updating",
      title_to_be_updated,
      date_to_be_updated,
      description_to_be_updated
    );

    const body = {
      name: title_to_be_updated,
      description: description_to_be_updated,
      deadline: date_to_be_updated,
      task_id: props.task_id,
    };
    await fetch("http://localhost:8080/update_task", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
  }, [
    title_to_be_updated,
    date_to_be_updated,
    description_to_be_updated,
    props.task_id,
  ]);

  const handle_name_change = (event: React.ChangeEvent<HTMLInputElement>) => {
    set_title_to_be_updated(event.target.value);
  };
  const handle_date_change = (newValue: Date | null) => {
    set_date_to_be_updated(newValue);
  };
  const handle_description_change = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    set_description_to_be_updated(event.target.value);
  };

  return (
    <div className={AppStyles.task_main}>
      {updating_the_task ? (
        <Fragment>
          <CardContent>
            <Typography variant="h5" component="div">
              Updating Task
            </Typography>
            <Typography variant="h5" component="div">
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Name"
                type="name"
                fullWidth
                variant="standard"
                defaultValue={props.name}
                onChange={handle_name_change}
              />
            </Typography>
            <Typography variant="body2">
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Description"
                type="type"
                fullWidth
                variant="standard"
                defaultValue={props.description}
                onChange={handle_description_change}
              />
            </Typography>
            <div className={AppStyles.seperator}>
              <DateTimePicker
                label="Deadline"
                value={date_to_be_updated}
                onChange={handle_date_change}
                renderInput={(params) => <TextField {...params} />}
              />
            </div>
          </CardContent>
          <CardActions>
            <Button
              variant="outlined"
              onClick={() => {
                props.confirm_update();
                handle_update_confirm();
                finish_updating_task();
              }}
            >
              Confirm
            </Button>
            <Button variant="outlined" onClick={finish_updating_task}>
              Cancel
            </Button>
          </CardActions>
        </Fragment>
      ) : (
        <Fragment>
          {props.batch_edit_mode === true ? (
            <Checkbox onClick={props.batch_select_onclick} />
          ) : (
            false
          )}
          <CardContent>
            <Typography variant="h5" component="div">
              {props.name}
            </Typography>
            <TIME_HANDLER deadline={props.deadline}></TIME_HANDLER>
            <Typography variant="body2">{props.description}</Typography>
          </CardContent>
          {props.batch_edit_mode === false ? (
            <CardActions>
              <COMPLETE_TASK
                onClick={props.onclick_complete_task}
              ></COMPLETE_TASK>
              <UPDATE_TASK onClick={start_update_the_task}></UPDATE_TASK>
              <MOVE_TASK onClick={props.onclick_move_task}></MOVE_TASK>
              <DELETE_TASK onClick={props.onclick_delete_task}></DELETE_TASK>
            </CardActions>
          ) : (
            false
          )}
        </Fragment>
      )}
    </div>
  );
}

export default TASK;
