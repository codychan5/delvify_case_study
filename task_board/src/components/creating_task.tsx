import {
  Button,
  CardActions,
  CardContent,
  TextField,
  Typography,
} from "@material-ui/core";
import { DateTimePicker } from "@material-ui/lab";
import { Fragment, useCallback, useState } from "react";
import AppStyles from "./Task.module.css";

function CREATING_TASK(props: {
  onclick_confirm: () => void;
  onclick_cancel: () => void;
  list_id: number;
}) {
  const [title_to_be_created, set_title_to_be_created] = useState("");
  const [date_to_be_created, set_date_to_be_created] = useState<Date | null>(
    null
  );
  const [description_to_be_created, set_description_to_be_created] =
    useState("");

  const handle_submit = useCallback(async () => {
    console.log(
      "submitting",
      title_to_be_created,
      date_to_be_created,
      description_to_be_created
    );

    const body = {
      name: title_to_be_created,
      description: description_to_be_created,
      deadline: date_to_be_created,
      list_id: props.list_id,
    };
    await fetch("http://localhost:8080/create_task", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
  }, [title_to_be_created, date_to_be_created, description_to_be_created,props.list_id]);

  const handle_name_change = (event: React.ChangeEvent<HTMLInputElement>) => {
    set_title_to_be_created(event.target.value);
  };
  const handle_date_change = (newValue: Date | null) => {
    set_date_to_be_created(newValue);
  };
  const handle_description_change = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    set_description_to_be_created(event.target.value);
  };

  return (
    <div className={AppStyles.task_main}>
      <Fragment>
        <CardContent>
          <Typography variant="h5" component="div">
            Creating New Task
          </Typography>
          <Typography variant="h5" component="div">
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Name"
              type="name"
              fullWidth
              variant="standard"
              onChange={handle_name_change}
            />
          </Typography>
          <Typography variant="body2">
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Description"
              type="text"
              fullWidth
              variant="standard"
              onChange={handle_description_change}
            />
          </Typography>
          <div className={AppStyles.seperator}>
            <DateTimePicker
              label="Deadline"
              value={date_to_be_created}
              onChange={handle_date_change}
              renderInput={(params) => <TextField {...params} />}
            />
          </div>
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            onClick={() => {
              handle_submit();
              props.onclick_confirm();
            }}
          >
            Confirm
          </Button>
          <Button variant="outlined" onClick={props.onclick_cancel}>
            Cancel
          </Button>
        </CardActions>
      </Fragment>
    </div>
  );
}

export default CREATING_TASK;
