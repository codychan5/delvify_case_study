export type list_type = {
  id: number;
  name: string;
};

export type completed_task_type =
  | never
  | {
      id: number;
      name: string;
      description: string;
      deadline: Date;
    };

export type task_type =
  | never
  | {
      id: number;
      name: string;
      description: string;
      deadline: Date;
    };
