import { Button } from '@material-ui/core';

function BATCH_EDIT(props: {
  onClick: () => void}
) {
  return (
    <Button variant="contained" onClick={props.onClick}>Batch Edit</Button>
  );
}

export default BATCH_EDIT;
