import { Button } from "@material-ui/core";

function UPDATE_TASK(props: { onClick: () => void }) {
  return (
    <Button variant="outlined" onClick={props.onClick}>
      Update Task
    </Button>
  );
}

export default UPDATE_TASK;
