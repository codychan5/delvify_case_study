import { Button } from "@material-ui/core";

function COMPLETE_TASK(props: { onClick: () => void }) {
  return (
    <Button variant="outlined" onClick={props.onClick}>
      Complete Task
    </Button>
  );
}

export default COMPLETE_TASK;
