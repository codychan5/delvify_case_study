import { Fab } from "@material-ui/core";
import { IoIosAdd } from "react-icons/io";
import AppStyles from "./Buttons.module.css";


function CREATE_LIST(props: {
  onClick: () => void}) {
  return (
    <Fab color="primary" aria-label="add" onClick={props.onClick}>
      <IoIosAdd className={AppStyles.create_list}/>
    </Fab>
  );
}

export default CREATE_LIST;
