import { Button } from '@material-ui/core';

function CREATE_TASK(props: {
  onClick: () => void}) {
  return (
    <Button variant="contained" color="success" onClick={props.onClick}>Create task</Button>
  );
}

export default CREATE_TASK;
