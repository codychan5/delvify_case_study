import { Button } from '@material-ui/core';

function BATCH_MOVE_TASKS(props: {
  onClick: () => void}) {
  return (
    <Button variant="contained" color="success" onClick={props.onClick}>Move Selected Tasks</Button>
  );
}

export default BATCH_MOVE_TASKS;
