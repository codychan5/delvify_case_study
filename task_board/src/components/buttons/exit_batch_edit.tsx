import { Button } from '@material-ui/core';

function EXIT_BATCH_EDIT(props: {
  onClick: () => void}
) {
  return (
    <Button variant="contained" onClick={props.onClick}>Exit Batch Edit Mode</Button>
  );
}

export default EXIT_BATCH_EDIT;
