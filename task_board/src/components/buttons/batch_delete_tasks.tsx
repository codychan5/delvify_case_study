import { Button } from '@material-ui/core';

function BATCH_DELETE_TASK(props: {
  onClick: () => void}) {
  return (
    <Button variant="contained" color="error" onClick={props.onClick}>Delete Selected Tasks</Button>
  );
}

export default BATCH_DELETE_TASK;
