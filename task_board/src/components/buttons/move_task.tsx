import { Button } from "@material-ui/core";

function MOVE_TASK(props: { onClick: () => void }) {
  return (
    <Button variant="outlined" onClick={props.onClick}>
      Move Task
    </Button>
  );
}

export default MOVE_TASK;
