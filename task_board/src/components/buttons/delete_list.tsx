import { Button } from '@material-ui/core';

function DELETE_LIST(props: {
  onClick: () => void}) {
  return (
    <Button variant="contained" color="error" onClick={props.onClick}>Delete the list</Button>
  );
}

export default DELETE_LIST;
