import { Button } from '@material-ui/core';

function DELETE_TASK(props:{
  onClick: () => void;
}) {
  return (
    <Button variant="outlined" onClick={props.onClick}>Delete Task</Button>
  );
}

export default DELETE_TASK;
