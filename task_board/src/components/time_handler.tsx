import { Typography } from "@material-ui/core";
import {useCallback } from "react";

function TIME_HANDLER(props: { deadline: Date }) {
  const date_time_handler = useCallback(() => {
    const d = new Date(props.deadline);
    return d.toLocaleString("en-US", { timeZone: "Asia/Hong_Kong" });
  }, [props.deadline]);

  return (
    <Typography sx={{ mb: 1.5 }} color="text.secondary">
      Deadline: {date_time_handler()}
    </Typography>
  );
}

export default TIME_HANDLER;
