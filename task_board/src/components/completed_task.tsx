import { CardContent, Typography } from "@material-ui/core";
import { Fragment } from "react";
import AppStyles from "./Task.module.css";
import TIME_HANDLER from "./time_handler";

function COMPLETED_TASK(props: {
  name: string;
  description: string;
  deadline: Date;
  key: number;
}) {
  return (
    <div className={AppStyles.task_main}>
      <Fragment>
        <CardContent>
          <Typography variant="h5" component="div">
            {props.name}
          </Typography>
          <TIME_HANDLER deadline={props.deadline}></TIME_HANDLER>
          <Typography variant="body2">{props.description}</Typography>
        </CardContent>
      </Fragment>
    </div>
  );
}

export default COMPLETED_TASK;
