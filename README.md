There's two main file at the repository, the "backend" file that stores all the source codes for backend related codes i.e. Node.js and databases, where the "task_board" file stores all the frontend / client side sources codes written in React.js.

1. To start, please cd into both "task_board" and "backend" file, run "yarn install" command to install all dependecies.
2. I am using PostgreSQL for database, you may install it if you haven't yet. You shall create a database called "task_board" within PostgreSQL.
3. Within the "backend" file, run "yarn knex migrate:latest" to create all the neccessary tables for the database.
4. Within the "backend" file, copy the ".env.example" file and rename it into ".env", and then go into this file and input your configs such as your DB name and password, etc.
5. within the "backend" file, run "yarn start" to start the backend server.
6. Then run "yarn start" command within the "task_board" file to start the server.
7. Then go on your browser with the URL: "http://localhost:3000/" to interact with the application.

For jest Testing:
1. Create another separate databse named "task_board_test" for jest testing.
2. within the "backend" file, run the command "yarn test" to run jest test.
3. Examine the test results.
